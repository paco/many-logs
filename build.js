const gen = require("hyperid")({ urlSafe: true });
const del = require("del").sync;
const { join } = require("path");
const { writeFileSync: write, mkdirSync: mkdir } = require("fs");

// output directory
const OUT_DIR = join(__dirname, "public");

// how many files to generate
const MAX_FILES = 3000;

// the prefix of the generated files / content
// replace with something random to trigger rotation
// of all the generated files
const FILE_PREFIX = "some-file";

// how many files to modify to trigger delta updates tests
const MAX_MODIFY = 1;

// Sleep for 5 seconds so I can open the deployment inspector lol
sleep(5000)

// cleanup existing filesystem state if any
del(OUT_DIR);
mkdir(OUT_DIR);

// state variables
const files = [];
let maxModified = MAX_MODIFY;

// generate all files
for (let i = 0; i < MAX_FILES; i++) {
  const fileName = `${FILE_PREFIX}-${i}.html`;
  const modify = maxModified && maxModified--;
  write(join(OUT_DIR, fileName), `<h1>${fileName}${Math.random()}</h1>${modify ? gen() : ""}`);
  files.push(fileName);
  process.stdout.write(`${modify ? "@" : "."}: ${fileName}\n`);
}

// generate an index file with a list
write(
  join(OUT_DIR, "index.html"),
  `<!doctype html>
<h1>List of built files</h1>
<style>
  * { margin: 0; padding: 0; }

  body {
    max-width: 900px;
    padding: 20px;
  }

  ul {
    column-count: 3;
    list-style: none;
  }

  ul li {
    display: inline-block;
  }
</style>
<ul>
  ${files.map(file => `<li><a href="${file}">${file}</a></li>`).join("")}
</ul>
`
);

function sleep(ms) {
  var start = new Date().getTime(), expire = start + ms;
  while (new Date().getTime() < expire) { }
  return;
}
